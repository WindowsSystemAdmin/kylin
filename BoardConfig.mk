#
# Copyright 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Arm32 device.
TARGET_ARCH := arm
TARGET_ARCH_VARIANT := armv7-a
TARGET_CPU_VARIANT := generic
TARGET_CPU_ABI := armeabi-v7a
TARGET_CPU_ABI2 := armeabi

TARGET_NO_BOOTLOADER := false
TARGET_NO_KERNEL := false

BOARD_KERNEL_CMDLINE := console=ttyS2,115200n8 androidboot.console=ttyS2 androidboot.hardware=rk3036 earlyprintk androidboot.selinux=enforcing

# Set up the local kernel.
TARGET_KERNEL_ARCH := $(TARGET_ARCH)
ifeq ($(RK_KERNEL_44), true)
TARGET_KERNEL_SRC := hardware/bsp/kernel/common/v4.4
else
TARGET_KERNEL_SRC := hardware/bsp/kernel/common/v4.1
endif
TARGET_KERNEL_DEFCONFIG := rk3036_kylin_defconfig
TARGET_KERNEL_CONFIGS := $(TARGET_KERNEL_CONFIGS) $(PWD)/device/rockchip/kylin/soc/soc.kconf
TARGET_KERNEL_DTB := rk3036-kylin.dtb
TARGET_KERNEL_DTB_APPEND := true

TARGET_BOARD_INFO_FILE := device/rockchip/kylin/board-info.txt

TARGET_USERIMAGES_USE_EXT4 := true
BOARD_FLASH_BLOCK_SIZE := 512
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 857734656
BOARD_USERDATAIMAGE_PARTITION_SIZE := 5762956288

BOARD_SEPOLICY_DIRS += device/rockchip/kylin/sepolicy

# We have to add all these product configurations here
# because bdk build doesn't read vendor specific product configs.

PRODUCT_MANUFACTURER := Rockchip

TARGET_BOARD_PLATFORM := rk3036
TARGET_BOOTLOADER_BOARD_NAME := kylin

# Clean up this.
soc_vendor := rockchip
soc_name := rk3036

# Add audio support
$(call add_peripheral, rockchip, audio/generic)
# Add bt controller
USE_BLUETOOTH_BCM4343 := true
$(call add_peripheral, rockchip, bluetooth/ap6212)
# Add wifi controller
$(call add_peripheral, rockchip, wifi/ap6212)

PRODUCT_COPY_FILES += \
    device/rockchip/kylin/init.rk3036.rc:root/init.rk3036.rc \
    system/core/rootdir/init.usb.rc:root/init.usb.rc \
    system/core/rootdir/init.usb.configfs.rc:root/init.usb.configfs.rc \
    device/rockchip/kylin/init.usb.rk3036.rc:root/init.usb.rk3036.rc \
    system/core/rootdir/ueventd.rc:root/ueventd.rc \
    device/rockchip/kylin/ueventd.rk3036.rc:root/ueventd.rk3036.rc \
    device/rockchip/kylin/fstab:root/fstab.${soc_name} \
    device/rockchip/kylin/provision-device:provision-device

DEVICE_PACKAGES += \
    lights.$(TARGET_BOARD_PLATFORM) \
    bt_bcm_ap6212 \
    bootctrl.$(TARGET_BOARD_PLATFORM)

# Default Keystore HAL
DEVICE_PACKAGES += \
    keystore.default \

# Must be defined at the end of the file
$(call add_device_packages)
